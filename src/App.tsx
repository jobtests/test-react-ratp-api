import React, { useState, useEffect, useCallback } from 'react';
import axios, { AxiosError } from 'axios';

import { LocationsTable } from './components/table/LocationsTable';
import { Pagination } from './components/pagination/Pagination';
import { Input } from './components/form/input/Input';
import { IApiData, ApiSorting } from './types/ratp-api';
import { API_URL, DISPLAYED_RECORDS_PER_PAGE } from './settings';

import './App.css';


function App() {
	const [error, setError] = useState<AxiosError | null>(null);
	const [isLoaded, setIsLoaded] = useState<boolean>(false);
	const [apiData, setApiData] = useState<IApiData>({} as IApiData);
	const [pageCount, setPageCount] = useState<number>(0);
	const [currentPage, setCurrentPage] = useState<number>(0);
	const [search, setSearch] = useState<string>('');
	const [sorting, setsorting] = useState<ApiSorting>('code_postal');

	useEffect(() => {
		const fetchData = async () => {
			try {
				const searchFilter = search ? `&q=${search}` : '';
				const response = await axios.get<IApiData>(`${API_URL + searchFilter}&start=${currentPage * DISPLAYED_RECORDS_PER_PAGE}&sort=${sorting}`);

				setApiData(response.data);
				setPageCount(Math.round(response.data.nhits / DISPLAYED_RECORDS_PER_PAGE) - 1);
				setIsLoaded(true);

			} catch (err) {
				setError(err as AxiosError);
				setIsLoaded(true);
			}
		};

		fetchData();
	}, [currentPage, sorting, search]);

	const handleInputChange = useCallback((e: React.FormEvent<HTMLInputElement>) => {
		setSearch(e.currentTarget.value);
		setCurrentPage(0);
	}, []);


	if (error) {
		return <p>Something went wrong...</p>;

	} else if (!isLoaded) {
		return <p>Loading...</p>;

	} else {
		return (
			<div className='App'>
				<h1>Commerces de proximité</h1>

				<Input onChange={handleInputChange} />

				<div className='App-table-container' style={{
					minHeight: 50 + (35 * DISPLAYED_RECORDS_PER_PAGE),
				}}>
					<LocationsTable data={apiData} sort={(sort) => setsorting(sort)} />
				</div>

				<Pagination
					current={currentPage}
					lastPage={pageCount}
					onClick={(newPageNumber) => setCurrentPage(newPageNumber)}
				/>
			</div>
		);
	}

}

export default App;
