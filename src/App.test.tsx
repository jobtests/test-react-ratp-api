import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';


it('renders learn react link', () => {
	render(<App />);
	const linkElement = screen.getByText(/Commerces de proximité|Loading/i);
	expect(linkElement).toBeInTheDocument();
});
