import React from 'react';
import { Story, Meta } from '@storybook/react';

import { Table, ITableProps } from './Table';

export default {
	title: 'Components/Tables/Table',
	component: Table,
	argTypes: { sort: { action: 'Sorted' }},
} as Meta;

const Template: Story<ITableProps> = (args) => <Table {...args} />;

export const Default = Template.bind({});
Default.args = {
	colNamesData: [
		{
			label: 'Col 1',
			value: 'col1',
			sortable: false,
		},
		{
			label: 'Col 2',
			value: 'col2',
			sortable: false,
		},
	],
	rowsData: {
		1: [
			{
				col: 'col1',
				content: 'Cell 1',
			},
			{
				col: 'col2',
				content: 'Cell 2',
			},
		],
		2: [
			{
				col: 'col1',
				content: 'Cell 1',
			},
			{
				col: 'col2',
				content: 'Cell 2',
			},
		],
	},
};
