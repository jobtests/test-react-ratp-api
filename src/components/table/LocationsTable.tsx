import React, { useMemo } from 'react';

import { Table, ITableRowsData, ITableCellData } from './Table';
import { IThCellData } from './ThCell';
import { IApiData, ApiSorting } from '../../types/ratp-api';

import './LocationsTable.css';


export interface ILocationsTableProps {
	data: IApiData,
	sort: (sort: ApiSorting) => void;
}

export const LocationsTable = ({ data, sort }: ILocationsTableProps) => {
	const colNamesData: IThCellData[] = [
		{
			label: 'Ville',
			value: 'ville',
			sortable: false,
		},
		{
			label: 'Libellé',
			value: 'tco_libelle',
			sortable: false,
		},
		{
			label: 'Code Postal',
			value: 'code_postal',
			sortable: true,
		},
	];

	const rowsData: ITableRowsData = useMemo(() => Object.values(data.records).reduce((acc, record) => {
		const { recordid, fields } = record;

		return {
			...acc,
			[recordid]: Object.entries(fields).reduce((acc: ITableCellData[], field) => {
				const colName = field[0];

				// Filter to display only these columns
				if (colName === 'ville' || colName === 'tco_libelle' || colName === 'code_postal') {
					return [
						...acc,
						{
							col: colName,
							content: field[1],
						},
					];
				} else {
					return acc;
				}
			}, []),
		};
	}, {}), [data.records]);

	// console.log('RENDER table');


	return (
		<Table
			className='LocationTable'
			colNamesData={colNamesData}
			rowsData={rowsData}
			sort={(val) => sort(val as ApiSorting)}
		/>
	);
}