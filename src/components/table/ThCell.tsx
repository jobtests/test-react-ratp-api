import React from 'react';
import classNames from 'classnames';
import { FaSortAmountUp, FaSortAmountDown } from 'react-icons/fa';

import { Button } from '../button/Button';

import './ThCell.css';


export interface IThCellData {
	label: string,
	value: string,
	sortable: boolean,
}

export interface IThCellProps {
	children?: React.ReactNode;
	isSortable?: boolean;
	sortDirection?: 'asc' | 'desc';
	onClick?: () => void;
}

export const ThCell = ({
	children,
	isSortable = false,
	sortDirection = 'asc',
	onClick,
}: IThCellProps) => {

	const cx = classNames('ThCell', { 'mod-clickable': isSortable });
	const sortIcon = sortDirection === 'asc' ? <FaSortAmountUp /> : <FaSortAmountDown />;
	const sortIconWrapper = <span className='ThCell-icon'>{sortIcon}</span>;

	return (
		<th className={cx} data-testid='test-th'>
			<Button className='ThCell-button' isDisabled={!isSortable} onClick={onClick}>
				{children}
				{isSortable ? sortIconWrapper : null}
			</Button>
		</th>
	);
};
