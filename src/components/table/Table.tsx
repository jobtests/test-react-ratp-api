import React, { useMemo, useState } from 'react';
import classNames from 'classnames';

import { ThCell, IThCellData } from './ThCell';

import './Table.css';


export interface ITableRowsData {
	[key: number]: ITableCellData[];
}

export interface ITableCellData {
	col: string;
	content: string | number;
}

export interface ITableProps {
	className?: string;
	colNamesData?: IThCellData[];
	rowsData: ITableRowsData;
	sort?: (sort: string) => void;
}

export const Table = ({
	className,
	colNamesData,
	rowsData,
	sort,
}: ITableProps) => {
	const [ascSort, setAscSort] = useState<boolean>(false);


	const thead = useMemo(() => {
		const handleSort = (colName: string) => {
			if (sort) {
				const sortLabel = ascSort ? colName : `-${colName}`;
				sort(sortLabel);
				setAscSort(!ascSort);
			}
		};

		if (colNamesData) {
			const cells = colNamesData.map((colNameData, i) => (
				<ThCell
					key={`thead-cell-${i}`}
					isSortable={colNameData.sortable}
					sortDirection={ascSort ? 'asc' : 'desc'}
					onClick={() => handleSort(colNameData.value)}
				>
					{colNameData.label}
				</ThCell>
			));

			return (
				<thead>
					<tr>
						{cells}
					</tr>
				</thead>
			);
		} else {
			return null;
		}
	}, [ascSort, colNamesData, sort]);

	const cx = classNames('Table', className?.split(' '));

	const renderCells = Object.entries(rowsData).map(data => {
		const rowId = data[0];
		const rowData: ITableCellData = data[1];

		const cells = Object.values(rowData).map((data: ITableCellData) => (
			<td key={`cell-${rowId}-${data.col}`}>{data.content}</td>
		));

		return (
			<tr key={`row-${rowId}`}>
				{cells}
			</tr>
		);
	});

	return (
		<table className={cx} data-testid='test-table'>
			{thead}
			<tbody>
				{renderCells}
			</tbody>
		</table>
	);
};
