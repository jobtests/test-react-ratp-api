import React from 'react';
import {
	cleanup,
	render,
	screen
} from '@testing-library/react';
import { composeStories } from '@storybook/testing-react';

import * as stories from '../Table.stories';

const { Default } = composeStories(stories);


afterEach(cleanup);

it('should render a table', () => {
	render(<Default />);
	expect(screen.getByTestId('test-table')).toBeVisible();
});

it('matches snapshot', () => {
	const tree = render(<Default />);
	expect(tree).toMatchSnapshot();
});
