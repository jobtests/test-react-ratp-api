import React from 'react';
import { Story, Meta } from '@storybook/react';

import { ThCell, IThCellProps } from './ThCell';

export default {
	title: 'Components/Tables/ThCell',
	component: ThCell,
} as Meta;

const Template: Story<IThCellProps> = (args) => <ThCell {...args} />;

export const Default = Template.bind({});
Default.args = {
	children: 'Header Cell',
};

export const SortableAsc = Template.bind({});
SortableAsc.args = {
	...Default.args,
	isSortable: true,
	sortDirection: 'asc',
};

export const SortableDesc = Template.bind({});
SortableDesc.args = {
	...SortableAsc.args,
	sortDirection: 'desc',
};
