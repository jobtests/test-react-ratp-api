import React from 'react';
import { Story, Meta } from '@storybook/react';

import { Button, IButtonProps } from './Button';

export default {
	title: 'Components/Button',
	component: Button,
	argTypes: { onClick: { action: 'Clicked' }},
} as Meta;

const Template: Story<IButtonProps> = (args) => <Button {...args} />;

export const Default = Template.bind({});
Default.args = {
	children: 'Button',
};

export const Active = Template.bind({});
Active.args = {
	...Default.args,
	isActive: true,
};

export const Disabled = Template.bind({});
Disabled.args = {
	...Default.args,
	isDisabled: true,
};
