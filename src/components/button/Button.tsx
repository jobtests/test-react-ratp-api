import React from 'react';
import classNames from 'classnames';

import './Button.css';


export interface IButtonProps {
	className?: string;
	children: React.ReactNode;
	isActive?: boolean;
	isDisabled?: boolean;
	onClick?: () => void;
}

export const Button = ({
	className,
	children,
	isActive,
	isDisabled,
	onClick,
}: IButtonProps) => {

	const customClasses = className ? className.split(' ') : null;
	const cx = classNames(
		'Button',
		{ 'mod-active': isActive },
		{ 'mod-disabled': isDisabled },
		customClasses,
	);

	return (
		<button
			className={cx}
			disabled={isDisabled}
			onClick={onClick}
			data-testid='test-button'
		>
			{children}
		</button>
	);
};