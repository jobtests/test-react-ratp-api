import React from 'react';
import { Story, Meta } from '@storybook/react';

import { Pagination, IPaginationProps } from './Pagination';

export default {
	title: 'Components/Pagination',
	component: Pagination,
	argTypes: { onClick: { action: 'Clicked' }},
} as Meta;

const Template: Story<IPaginationProps> = (args) => <Pagination {...args} />;

export const Default = Template.bind({});
Default.args = {
	lastPage: 10,
	current: 5,
};
