import React, { useMemo } from 'react';
import {
	FaAngleDoubleLeft,
	FaAngleDoubleRight,
	FaAngleLeft,
	FaAngleRight,
} from "react-icons/fa";

import { Button } from '../button/Button';

import './Pagination.css';


export interface IPaginationProps {
	firstPage?: number;
	lastPage: number;
	current: number;
	onClick: (pageNumber: number) => void;
}

export const Pagination = ({
	firstPage = 0,
	lastPage,
	current,
	onClick,
}: IPaginationProps) => {

	const displayedPages = 2;
	const pages = useMemo(() => {
		let prevPagesBtn = [];
		for (let i = current - displayedPages; i < current; i++) {
			if (i >= 0) {
				prevPagesBtn.push(<Button key={`prev-page-${i}`} onClick={() => onClick(i)}>{i + 1}</Button>);
			} else { // These empty divs are placeholders to keep this section always the same width
				prevPagesBtn.push(<div key={`prev-page-${i}`} />);
			}
		}
		let nextPagesBtn = [];
		for (let i = current + 1; i <= current + displayedPages; i++) {
			if (i <= lastPage) {
				nextPagesBtn.push(<Button key={`next-page-${i}`} onClick={() => onClick(i)}>{i + 1}</Button>);
			}
		}

		return (
			<div className='Pagination-page-numbers'>
				{prevPagesBtn}
				<Button isActive>{current + 1}</Button>
				{nextPagesBtn}
			</div>
		);
	}, [current, lastPage, onClick]);

	console.log('RENDER Pagination');

	return (
		<div className='Pagination' data-testid='test-pagination'>
			<div className='Pagination-prev'>
				<Button
					isDisabled={current === firstPage}
					onClick={() => onClick(firstPage)}
				>
					<FaAngleDoubleLeft />
				</Button>
				<Button
					isDisabled={current === firstPage}
					onClick={() => onClick(current - 1)}
				>
					<FaAngleLeft />
				</Button>
			</div>

			{pages}

			<div className='Pagination-next'>
				<Button
					isDisabled={current === lastPage}
					onClick={() => onClick(current + 1)}
				>
					<FaAngleRight />
				</Button>
				<Button
					isDisabled={current === lastPage}
					onClick={() => onClick(lastPage)}
				>
					<FaAngleDoubleRight />
				</Button>
			</div>
		</div>
	);
};