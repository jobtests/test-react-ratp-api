import React from 'react';

import './Input.css';


export interface IInputProps {
	onChange: (e: React.FormEvent<HTMLInputElement>) => void;
}

export const Input = ({
	onChange,
}: IInputProps) => {
	return (
		<input
			className='Input'
			placeholder='Search'
			onChange={onChange}
			data-testid='test-input'
		/>
	);
};
