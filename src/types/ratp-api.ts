export interface IApiData {
	nhits: number;
	parameters: IApiParameters;
	records: IApiRecord[];
	facet_groups: IApiFacetgroup[];
}

export interface IApiFacetgroup {
	facets: IApiFacet[];
	name: string;
}

export interface IApiFacet {
	count: number;
	path: string;
	state: string;
	name: string;
}

export interface IApiRecord {
	datasetid: string;
	recordid: string;
	fields: IApiFields;
	record_timestamp: string;
}

export interface IApiFields {
	ville: string;
	tco_libelle: string;
	code_postal: number;
	dea_numero_rue_livraison_dea_rue_livraison: string;
	dea_fermeture?: string;
}

export interface IApiParameters {
	dataset: string;
	timezone: string;
	rows: number;
	start: number;
	sort: string[];
	format: string;
	facet: string[];
}

export type ApiSorting = 'code_postal' | '-code_postal';