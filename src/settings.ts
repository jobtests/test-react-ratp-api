export const API_URL = 'https://data.ratp.fr/api/records/1.0/search/?dataset=liste-des-commerces-de-proximite-agrees-ratp&q=&facet=dea_fermeture&facet=ville&rows=10';

export const DISPLAYED_RECORDS_PER_PAGE = 10;
